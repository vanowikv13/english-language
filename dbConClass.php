<?php
//remember to everytime close  or at least delete object to stop connection with database
class DatabaseConnection
{
    //connection with DB
    protected $conn;
    public $isConnected;

    protected $lastQueryResultData;

    //connection data
    private $host = 'localhost';
    private $dbname = 'learn_english';
    private $username = 'root';
    private $password = '';

    public function __construct()
    {
        $this->connect();
    }

    //checking if connection is stil working and didn't happend any errors
    public function errnoControl()
    {
        if(mysqli_connect_errno()) {
            $this->isConnected = false;
            return false;
        }
        return true;
    }

    //checking connection with DB
    public function pingControl()
    {
        if(!mysqli_ping($this->conn)) {
            $this->isConnected = false;
            return false;
        }
        return true;
    }

    //return number of rows from last querry
    public function lastQueryRows()
    {
        if($this->lastQueryResultData == false)
            return false;
        return mysqli_num_rows($this->lastQueryResultData);
    }

    //return true if there is stil connection with server
    public function isConnectedToServer()
    {
        if($this->isConnected && $this->pingControl())
            return true;
        return false;
    }

    //send query to server and return result of query if success if not return message about failure
    public function query($sqlQuerry)
    {
        if(!$this->isConnectedToServer()) return "connection with server is lose";
        if($sqlQuerry != null) {
            $this->lastQueryResultData = mysqli_query($this->conn, $sqlQuerry);
            return $this->lastQueryResultData;
        }
        return null;
    }

    //connect to a server
    protected function connect()
    {
        if(!$this->isConnectedToServer()) {
            $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->dbname);
            $this->isConnected = true;
            $this->errnoControl();
            $this->pingControl();
        }
    }

    //reconnect to a server
    public function reconnect()
    {
        $this->connect();
    }

    //return result of last query
    public function lastQueryResult()
    {
        return $this->lastQueryResultData;
    }

    //close connection with DB
    public function close()
    {
        if ($this->isConnectedToServer()) {
            mysqli_close($this->conn);
            $this->isConnected=false;
        }
    }

    public function __destruct()
    {
        $this->close();
    }
};

?>