<?php

include_once('dbConClass.php');

/*Main class that is making all operation on websites and it's managed by this class */
class DataOperation
{
    private $conn;

    public function __construct()
    {
        $this->conn = new DatabaseConnection();
    }

    //insert querry about polish sings
    protected function polishSings()
    {
        $this->conn->query("SET CHARSET utf8");
        $this->conn->query("SET NAMES `utf8` COLLATE `utf8_polish_ci`");
    }

    //check connection if there is any problem throw error
    protected function checkConnectionToServer()
    {
        if ($this->conn->isConnectedToServer() == false)
            $this->conn->reconnect();
        if($this->conn->isConnectedToServer() == false)
            throw new Message("Problem with access to database if you still have this problem latter write to our support center");

    }

    //insert categeory to DB category
    public function insertCategory($name, $description)
    {
        //store every word in DB as a lcase
        $name = strtolower($name);

        //chekc if category exist
        if ($this->getCategoryID($name))
            header("Location: index.php");

        //throw eror if it's not connected
        $this->checkConnectionToServer();

        //insert category if not exist with this $name in DB
        $this->conn->query("insert into category (name, description) VALUES ('" . $name . "'," . '"' . $description . '");');
        header("Location: index.php");
    }

    //function checking if words is in table if it's there return ID
    protected function getWordID($word, $country)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();

        //initialize values to check if it's polish or english
        $tableName = 'words_en';
        $idName = 'id_en';
        if ($country == 'pl' || $country == 'words_pl') {
            $tableName = 'words_pl';
            $idName = 'id_pl';
        }

        $this->polishSings();
        $this->conn->query("select {$idName} from {$tableName} where word = '{$word}'");

        if ($this->conn->lastQueryRows() < 0)
            return false;

        //if word exist return id of it
        return mysqli_fetch_assoc($this->conn->lastQueryResult())[$idName];
    }

    //return category id if exist in db or fals if not
    protected function getCategoryID($catName)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();

        if ($this->conn->query("select id_cat from category where name = '{$catName}'"))
            if ($this->conn->lastQueryRows() > 0)
            return mysqli_fetch_assoc($this->conn->lastQueryResult())['id_cat'];
        return false;
    }

    //return connection id if exsit if not return false
    protected function getConnectionID($id_cat, $id_pl, $id_en)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();

        $this->conn->query("select id_con from connections where id_pl = {$id_pl} and id_en = {$id_en} and id_cat = {$id_cat};");
        if ($this->conn->lastQueryRows() > 0)
            return mysqli_fetch_assoc($this->conn->lastQueryResult())['id_con'];
        return false;
    }

    //insert one word into table polish_words or english_words
    protected function insertWordToTableReturnID($word, $table)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();
        $this->polishSings();
        $this->conn->query("insert into {$table} (word) value ('" . $word . "')");
        return $this->getWordID($word, $table);
    }

    protected function addConnectionPolEng($id_cat, $id_pl, $id_en, $description = null)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();

        //insert new connection
        $this->conn->query("insert into connections(id_cat, id_pl, id_en, description) values ({$id_cat}, {$id_pl}, {$id_en}," . '"' . $description . '")');

        //check if connection was inserted well
        if ($this->getConnectionID($id_cat, $id_pl, $id_en))
            return true;
        return false;

    }

    //increase words number in category by one
    protected function increaseCategoryWordsNumber($name, $id_cat = null)
    {
        //throw eror if it's not connected
        $this->checkConnectionToServer();
        $sql = "update category set words = words + 1 where name = '{$name}' or id_cat = {$id_cat}";
        $this->conn->query("update category set words = words + 1 where name = '{$name}' or id_cat = {$id_cat}");
    }

    //process that improve insertion for words and return id of words in table
    protected function insertionProcessForWords($word, $table)
    {
        if ($table == 'words_pl')
            $id = $this->getWordID($word, 'pl');
        else
            $id = $this->getWordID($word, 'en');

        //if(words not exist) insert it and return id of it
        if ($id == false)
            $id = $this->insertWordToTableReturnID($word, $table);

        return $id;
    }

    //process that insert word to db all kind of action are started by this function
    public function insertionWordProces($english, $polish, $description, $category)
    {
        strtolower($polish); strtolower($english);
        $id_pol = $this->insertionProcessForWords($polish, 'words_pl');
        $id_eng = $this->insertionProcessForWords($english, 'words_en');

        //check if category exist and return index of it
        $id_cat = $this->getCategoryID($category);
        if ($id_cat == false)
            throw new Message("You chose category that is not in database please reload main page and add make your action again");

        //check if connection already exist
        if ($this->getConnectionID($id_cat, $id_pol, $id_eng))
            throw new Message("Connection between this words and category already exist in database");

        //Adding category to database
        if (!$this->addConnectionPolEng($id_cat, $id_pol, $id_eng, $description))
            throw new Message("Problem with adding connection between words, categories and description");

        //increase number of words in category
        $this->increaseCategoryWordsNumber($category, $id_cat);


    }

    public function __destruct()
    {
        $this->conn->close();
    }
}

?>