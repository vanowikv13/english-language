<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>englishLessons</title>
</head>

<body>
    <h1>Adding new words</h1>
    <form action="addWordProces.php" method="post">
        English word: <input type="text" name="english"><br>
        Polish word: <input type="text" name="polish"><br>
        <select name="category">
            <?php
    include_once('dbConClass.php');
    $conn = new DatabaseConnection();
    $sql = "select name from category";

    $result = $conn->query($sql);
    if ($conn->lastQueryRows() > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo "<option>" . $row["name"] . "</option>";
        }
    }
    $conn->close();
    ?></select><br>
        English description: <input type="text" name="description"><br>
        <input type="submit" value="Add">
    </form>
</body>

</html>