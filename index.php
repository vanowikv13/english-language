<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>English</title>
</head>
<body>
    <h1>Welcome in our big English words data base</h1></br>
    <h3>Here is our list of things you can do:</h3>
    <ul>
        <li><a href="addWord.php">Adding word to existing cathegory</a></li>
        <li><a href="addCategory.php">Adding new category</a></li>
        <li><a href="test.php">Make a test of some category</a></li>
        <li><a href="random.php">Get random words from random category</a></li>
        <li><a href="searchWords.php">Search in all the words we have in different category</a></li>
    </ul>
</body>
</html>